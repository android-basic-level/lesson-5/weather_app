package com.mygeekbrains.weather_app;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.mygeekbrains.weather_app.dataprovider.WeatherModel;

public class DetailActivity extends AppCompatActivity {

    public static final String DETAILS = "DETAILS";
    private static final String BASE_URL = "https://www.google.com/search?q=";

    private static final char DEGREE_CHAR = '\u2103';

    /* элементы управления */
    private TextView txtCity;
    private CheckBox chkWetness;
    private CheckBox chkOvercast;
    private ImageView imgWeatherIcon;
    private TextView txtTemperature;
    private TextView txtWetness;
    private TextView txtOvercast;

    private boolean isWetnessShown = false;
    private boolean isOvercastShown = false;

    private WeatherModel currentModel = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Intent intent = getIntent();
        this.currentModel = (WeatherModel) intent.getSerializableExtra(DETAILS);

        bindUiControls();
        restoreVariable(savedInstanceState);
        refreshUiControlsData();
    }

    private void bindUiControls() {
        txtCity = findViewById(R.id.txtCity);

        imgWeatherIcon = findViewById(R.id.weatherIcon);

        txtTemperature = findViewById(R.id.txtTemperature);

        txtWetness = findViewById(R.id.txtWetness);

        txtOvercast = findViewById(R.id.txtOvercast);

        chkWetness = findViewById(R.id.chkWetness);
        chkWetness.setOnClickListener(v -> {
            this.isWetnessShown = !this.isWetnessShown;
            txtWetness.setVisibility(isWetnessShown ? View.VISIBLE : View.INVISIBLE);
        });

        chkOvercast = findViewById(R.id.chkOvercast);
        chkOvercast.setOnClickListener(v -> {
            this.isOvercastShown = !this.isOvercastShown;
            txtOvercast.setVisibility(isOvercastShown ? View.VISIBLE : View.INVISIBLE);
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle stateToSave) {
        stateToSave.putBoolean(WeatherModel.KEY_IS_WETNESS_SHOWN, isWetnessShown);
        stateToSave.putBoolean(WeatherModel.KEY_IS_OVERCAST_SHOWN, isOvercastShown);

        stateToSave.putString(WeatherModel.KEY_CITY, this.currentModel.city);
        stateToSave.putInt(WeatherModel.KEY_TEMPERATURE, this.currentModel.temperature);
        stateToSave.putInt(WeatherModel.KEY_WETNESS, this.currentModel.wetness);
        stateToSave.putInt(WeatherModel.KEY_OVERCAST, this.currentModel.overcast);
        stateToSave.putString(WeatherModel.KEY_IMAGE, this.currentModel.image);

        super.onSaveInstanceState(stateToSave);
    }


    private void restoreVariable(Bundle stateToRestore) {
        if (stateToRestore == null) {
            return;
        }

        isWetnessShown = stateToRestore.getBoolean(WeatherModel.KEY_IS_WETNESS_SHOWN);
        isOvercastShown = stateToRestore.getBoolean(WeatherModel.KEY_IS_OVERCAST_SHOWN);

        this.currentModel = new WeatherModel(stateToRestore.getString(WeatherModel.KEY_CITY),
                stateToRestore.getInt(WeatherModel.KEY_TEMPERATURE),  stateToRestore.getInt(WeatherModel.KEY_WETNESS),
                stateToRestore.getInt(WeatherModel.KEY_OVERCAST), stateToRestore.getString(WeatherModel.KEY_IMAGE));

    }

    private void refreshUiControlsData() {
        if (this.currentModel == null) {
            return;
        }

        txtCity.setText(this.currentModel.city);
        txtCity.setOnClickListener(v -> {
            String url = BASE_URL + this.currentModel.city + " погода";
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);
        });

        String temperature = this.currentModel.temperature.toString() + DEGREE_CHAR;
        txtTemperature.setText(temperature);

        String wetness = this.currentModel.wetness.toString() + "%";
        txtWetness.setText(wetness);
        txtWetness.setVisibility(isWetnessShown ? View.VISIBLE : View.INVISIBLE);

        String overcast = this.currentModel.overcast.toString() + "%";
        txtOvercast.setText(overcast);
        txtOvercast.setVisibility(isOvercastShown ? View.VISIBLE : View.INVISIBLE);

        int drawableId = getResources().getIdentifier(this.currentModel.image, "drawable", getPackageName());
        Drawable image = getResources().getDrawable(drawableId);
        imgWeatherIcon.setImageDrawable(image);
    }

}
