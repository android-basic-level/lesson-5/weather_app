package com.mygeekbrains.weather_app.dataprovider;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class WeatherDataProvider {

    private static final String[] CITIES;

    private static final Map<String, WeatherModel> DATA;

    static {
        WeatherModel[] initData = new WeatherModel[6];

        initData[ 0 ] = new WeatherModel("Москва", -21, 60, 40, "snow");
        initData[ 1 ] = new WeatherModel("Ташкент", 7, 45, 22, "sun");
        initData[ 2 ] = new WeatherModel("Алматы", 1, 57, 43, "rain");
        initData[ 3 ] = new WeatherModel("Ашхабад", 14, 26, 32, "sun");
        initData[ 4 ] = new WeatherModel("Баку", 16, 45, 80, "sun_cloud");
        initData[ 5 ] = new WeatherModel("Ереван", 12, 98, 95, "storm");

        String[] cityList = new String[ initData.length ];
        int ndx = 0;
        Map<String, WeatherModel> map = new HashMap<>();

        for (WeatherModel model : initData) {
            map.put(model.city, model);
            cityList[ ndx++ ] = model.city;
        }

        DATA = map;
        CITIES = cityList;
    }

    public String[] getCities() {
        return Arrays.copyOf(CITIES, CITIES.length);
    }

    public WeatherModel getByCity(String city) {
        return DATA.get(city);
    }


}
