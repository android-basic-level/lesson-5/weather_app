package com.mygeekbrains.weather_app.dataprovider;

import java.io.Serializable;

public class WeatherModel implements Serializable {

    public static final String KEY_IS_WETNESS_SHOWN = "KEY_IS_WETNESS_SHOWN";
    public static final String KEY_IS_OVERCAST_SHOWN = "KEY_IS_OVERCAST_SHOWN";
    public static final String KEY_CITY = "KEY_CITY";
    public static final String KEY_TEMPERATURE = "KEY_TEMPERATURE";
    public static final String KEY_WETNESS = "KEY_WETNESS";
    public static final String KEY_OVERCAST = "KEY_OVERCAST";
    public static final String KEY_IMAGE = "KEY_IMAGE";

    public final String city;
    public final Integer temperature;
    public final Integer wetness;
    public final Integer overcast;
    public final String image;

    public WeatherModel(String city, Integer temperature, Integer wetness, Integer overcast, String image) {
        this.city = city;
        this.temperature = temperature;
        this.wetness = wetness;
        this.overcast = overcast;
        this.image = image;
    }
}
